import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";
import reducer from "./ProductRedux/reducer";
import itemReducer from "./ProductRedux/itemReducer";
import cartReducer from "./ProductRedux/cartReducer";

const createComposer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducers = combineReducers({
  productData: reducer,
  productItem: itemReducer,
  cartItem:cartReducer
});
const store = createStore(rootReducers, createComposer(applyMiddleware(thunk)));

export default store;
