import {
  FETCH_CART,
  FETCH_CART_FAILURE,
  FETCH_CART_SUCCESS,
  FETCH_PRODUCT_ITEM,
  FETCH_PRODUCT_ITEM_FAILURE,
  FETCH_PRODUCT_ITEM_SUCCESS,
  FETCH_PRODUCT_LIST,
  FETCH_PRODUCT_LIST_ERROR,
  FETCH_PRODUCT_LIST_SUCCESS,
  REMOVE_CART,
  REMOVE_CART_FAILURE,
  REMOVE_CART_SUCCESS,
  UPDATE_CART,
  UPDATE_CART_FAILURE,
  UPDATE_CART_SUCCESS,
} from "./constant";
import Commerce from "@chec/commerce.js";

const commerce = new Commerce(
  "pk_test_417200ce68214de9e5c9fe9a646a783337891e6032a28"
);

export const fetchProductRequest = () => {
  return {
    type: FETCH_PRODUCT_LIST,
  };
};

export const fetchProductSuccess = (payload) => {
  return {
    type: FETCH_PRODUCT_LIST_SUCCESS,
    payload,
  };
};

export const fetchProductFailure = (error) => {
  return {
    type: FETCH_PRODUCT_LIST_ERROR,
    error,
  };
};

export const fetchProductData =
  (search = "") =>
  (dispatch) => {
    dispatch(fetchProductRequest());

    return search.length
      ? commerce.products
          .list({ query: search })
          .then((res) => {
            console.log("response success", res);
            dispatch(fetchProductSuccess(res.data));
          })
          .catch((err) => dispatch(fetchProductFailure(err)))
      : commerce.products
          .list()
          .then((res) => {
            console.log("response success", res);
            dispatch(fetchProductSuccess(res.data));
          })
          .catch((err) => dispatch(fetchProductFailure(err)));
  };

export const fetchProductItem = () => {
  return {
    type: FETCH_PRODUCT_ITEM,
  };
};

export const fetchProductItemSuccess = (payload) => {
  return {
    type: FETCH_PRODUCT_ITEM_SUCCESS,
    payload,
  };
};

export const fetchProductItemFailure = (error) => {
  return {
    type: FETCH_PRODUCT_ITEM_FAILURE,
    error,
  };
};

export const fetchProductItemData = (id) => (dispatch) => {
  dispatch(fetchProductItem());

  return commerce.products
    .retrieve(id)
    .then((res) => {
      // console.log("respo", res);
      dispatch(fetchProductItemSuccess(res));
    })
    .catch((err) => dispatch(fetchProductItemFailure(err)));
};

export const addCartItem = () => {
  return {
    type: FETCH_PRODUCT_ITEM,
  };
};

export const addCartItemSuccess = (payload) => {
  return {
    type: FETCH_PRODUCT_ITEM_SUCCESS,
    payload,
  };
};

export const addCartItemFailure = (error) => {
  return {
    type: FETCH_PRODUCT_ITEM_FAILURE,
    error,
  };
};

export const addCartItemData = (productId, quantity) => (dispatch) => {
  console.log(productId, quantity);
  dispatch(addCartItem());

  return commerce.cart
    .add(productId, quantity)
    .then((res) => {
      console.log("respo", res);
      dispatch(addCartItemSuccess(res));
    })
    .catch((err) => dispatch(addCartItemFailure(err)));
};

export const fetchCartItem = () => {
  return {
    type: FETCH_CART,
  };
};

export const fetchCartItemSuccess = (payload) => {
  return {
    type: FETCH_CART_SUCCESS,
    payload,
  };
};

export const fetchCartItemFailure = (error) => {
  return {
    type: FETCH_CART_FAILURE,
    error,
  };
};

export const fetchCartItemData = () => (dispatch) => {
  dispatch(fetchCartItem());
  return commerce.cart
    .retrieve()
    .then((res) => {
      console.log("respo", res);
      dispatch(fetchCartItemSuccess(res));
    })
    .catch((err) => dispatch(fetchCartItemFailure(err)));
};

export const removeCartItem = () => {
  return {
    type: REMOVE_CART,
  };
};

export const removeCartItemSuccess = (payload) => {
  return {
    type: REMOVE_CART_SUCCESS,
    payload,
  };
};

export const removeCartItemFailure = (error) => {
  return {
    type: REMOVE_CART_FAILURE,
    error,
  };
};

export const removeCartItemData = (id) => (dispatch) => {
  dispatch(removeCartItem());

  return commerce.cart
    .remove(id)
    .then((res) => {
      dispatch(removeCartItemSuccess(res));
    })
    .catch((err) => dispatch(removeCartItemFailure(err)));
};

export const updateCartItem = () => {
  return {
    type: UPDATE_CART,
  };
};

export const updateCartItemSuccess = (payload) => {
  return {
    type: UPDATE_CART_SUCCESS,
    payload,
  };
};

export const udpateCartItemFailure = (error) => {
  return {
    type: UPDATE_CART_FAILURE,
    error,
  };
};

export const updateCartItemData = (id, quantity) => (dispatch) => {
  dispatch(updateCartItem());

  return commerce.cart
    .update(id, { quantity: quantity })
    .then((res) => {
      dispatch(updateCartItemSuccess(res));
    })
    .catch((err) => dispatch(udpateCartItemFailure(err)));
};
