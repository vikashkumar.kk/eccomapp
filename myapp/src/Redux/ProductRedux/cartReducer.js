import {
  FETCH_CART,
  FETCH_CART_FAILURE,
  FETCH_CART_SUCCESS,
} from "./constant";

const initialState = {
  isLoading: false,
  error: "",
  cartList: [],
};

const cartReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_CART:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_CART_SUCCESS:
      return {
        ...state,
        isLoading: false,
        cartList: payload,
      };
    case FETCH_CART_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload,
      };
    default:
      return state;
  }
};

export default cartReducer;
