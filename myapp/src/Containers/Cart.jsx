import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchCartItemData,
  removeCartItemData,
  updateCartItemData,
} from "../Redux/ProductRedux/action";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import DeleteIcon from "@mui/icons-material/Delete";
import RemoveIcon from "@mui/icons-material/Remove";
import AddIcon from "@mui/icons-material/Add";

const Cart = () => {
  const [quantity, setQuantity] = useState(1);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCartItemData());
  }, []);

  const cartData = useSelector((state) => state.cartItem.cartList);
  const { line_items } = cartData;
  const cartItemDelete = (id) => {
    dispatch(removeCartItemData(id));
    dispatch(fetchCartItemData());
  };

  const handleUpdate = (id,counter) => {
    dispatch(updateCartItemData(id, counter));
    dispatch(fetchCartItemData());
  };

  return (
    <div>
      {line_items?.length ? (
        line_items?.map((item) => {
          return (
            <div
              key={item.id}
              style={{
                border: "1px solid red",
                width: "600px",
                margin: "auto",
              }}
            >
              <Typography
                sx={{ fontSize: 14 }}
                color="text.secondary"
                gutterBottom
              >
                {item.name}
              </Typography>

              <Typography sx={{ mb: 1.5 }} color="text.secondary">
                {item?.price?.formatted_with_symbol}
              </Typography>
              <Button
                disabled={quantity < 2}
                onClick={() => {
                  setQuantity(quantity - 1);
                  handleUpdate(item.id,quantity);
                }}
              >
                <RemoveIcon />
              </Button>

              {quantity}
              <Button
                size="small"
                onClick={() => {
                  setQuantity(quantity + 1);
                  handleUpdate(item.id,quantity);
                }}
              >
                <AddIcon />
              </Button>

              <Button size="small" onClick={() => cartItemDelete(item.id)}>
                <DeleteIcon />
              </Button>
            </div>
          );
        })
      ) : (
        <div style={{}}>
          <h2> List is Empty</h2>
        </div>
      )}
      <div>
        <p>
          Total order amount ={" "}
          <strong>{cartData?.subtotal?.formatted_with_symbol}</strong>
        </p>
      </div>
    </div>
  );
};
export default Cart;
