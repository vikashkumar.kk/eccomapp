import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProductData } from "../Redux/ProductRedux/action";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import ProductCard from "../Components/ProductCard";
import { Link } from "react-router-dom";

const ProductPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProductData());
  }, []);

  const productList = useSelector((state) => state.productData.productList);
  return (
    <div>
      Product
      <Box sx={{ flexGrow: 1, margin: "100px 100px" }}>
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ sm: 2, md: 4, lg: 4 }}
          justifyContent="center"
        >
          {productList?.map((ele, index) => (
            <Grid item key={ele.id}>
              <Link
                to={`/products/${ele.id}`}
                style={{ textDecoration: "none" }}
              >
                <ProductCard
                  prodImage={ele.image.url}
                  productName={ele.name}
                  productPrice={ele.price.formatted_with_symbol}
                />
              </Link>
            </Grid>
          ))}
        </Grid>
      </Box>
    </div>
  );
};

export default ProductPage;
